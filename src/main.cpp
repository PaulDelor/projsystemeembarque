#include <M5Core2.h>
int coloriage[] = {BLACK,WHITE,RED,BLUE,GREEN,YELLOW};
int coloriageIndex = 0;

void setup() 
{
    M5.begin();
    M5.Lcd.fillScreen(WHITE);
    M5.Lcd.fillRect(0,200, 320, 40, LIGHTGREY);
    M5.Lcd.fillRect(10,212, 30, 26, BLACK);
    M5.Lcd.fillRect(48,212, 30, 26, WHITE);
    M5.Lcd.fillRect(86, 212, 30, 26, RED);
    M5.Lcd.fillRect(124, 212, 30, 26, BLUE);
    M5.Lcd.fillRect(162, 212, 30, 26, GREEN);
    M5.Lcd.fillRect(200, 212, 30, 26, YELLOW);
    M5.Lcd.drawRect(10, 212, 30, 26, ORANGE);
    M5.Lcd.drawRect(11, 213, 28, 24, ORANGE);
}

void loop() {
    M5.update();
    TouchPoint_t coordinate;
    if(M5.Touch.ispressed()){
        coordinate = M5.Touch.getPressPoint();
        if(coordinate.y < 200){
            M5.Lcd.drawPixel(coordinate.x,coordinate.y,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x+1,coordinate.y,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x-1,coordinate.y,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x,coordinate.y+1,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x,coordinate.y-1,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x+1,coordinate.y+1,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x-1,coordinate.y-1,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x-1,coordinate.y+1,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x+1,coordinate.y-1,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x+2,coordinate.y,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x-2,coordinate.y,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x,coordinate.y+2,coloriage[coloriageIndex]);
            M5.Lcd.drawPixel(coordinate.x,coordinate.y-2,coloriage[coloriageIndex]);
        }
    }
    if (M5.BtnC.wasPressed()) {
        M5.Lcd.setCursor(80,200);
        M5.Lcd.println("Button C is Pressed.");
        M5.Lcd.fillRect(10+38*coloriageIndex,212, 30, 26, coloriage[coloriageIndex]);
        coloriageIndex++;
        if(coloriageIndex >= 6){
            coloriageIndex = 0;
        }
        M5.Lcd.drawRect(10+38*coloriageIndex,212, 30, 26, ORANGE);
        M5.Lcd.drawRect(11+38*coloriageIndex,213, 28, 24, ORANGE);
    if (M5.BtnA.wasPressed()) {
        M5.Lcd.setCursor(80,200);
        M5.Lcd.println("Button A is Pressed.");
        M5.Lcd.fillRect(10+38*coloriageIndex,212, 30, 26, coloriage[coloriageIndex]);
        coloriageIndex--;
        if(coloriageIndex < 0){
            coloriageIndex = 5;
        }
        M5.Lcd.drawRect(10+38*coloriageIndex,212, 30, 26, ORANGE);
        M5.Lcd.drawRect(11+38*coloriageIndex,213, 28, 24, ORANGE);
    }
    if (M5.BtnB.pressedFor(1500)&& !M5.BtnB.pressedFor(1520)) {
        M5.Lcd.setCursor(80,200);
        M5.Lcd.println("Button B is Pressed.");
        M5.Lcd.fillScreen(coloriage[coloriageIndex]);
        M5.Lcd.fillRect(0,200, 320, 40, LIGHTGREY);
        M5.Lcd.fillRect(10,212, 30, 26, BLACK);
        M5.Lcd.fillRect(48,212, 30, 26, WHITE);
        M5.Lcd.fillRect(86, 212, 30, 26, RED);
        M5.Lcd.fillRect(124, 212, 30, 26, BLUE);
        M5.Lcd.fillRect(162, 212, 30, 26, GREEN);
        M5.Lcd.fillRect(200, 212, 30, 26, YELLOW);
        M5.Lcd.drawRect(10+38*coloriageIndex,212, 30, 26, ORANGE);
        M5.Lcd.drawRect(11+38*coloriageIndex,213, 28, 24, ORANGE);
        M5.Lcd.fillRect(10+38*coloriageIndex,212, 30, 26, coloriage[coloriageIndex]);
        coloriageIndex++;
        if(coloriageIndex >= 6){
            coloriageIndex = 0;
        }
        M5.Lcd.drawRect(10+38*coloriageIndex,212, 30, 26, ORANGE);
        M5.Lcd.drawRect(11+38*coloriageIndex,213, 28, 24, ORANGE);

    }
    delay(20);
}